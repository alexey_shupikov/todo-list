# README #

### Client installation in client/README.md ###

* used Vue.js
* live - https://adept-primer-277523.ew.r.appspot.com/

### Server installation in server/README.md ###

* used Flask
* live - https://todo-list-server.ew.r.appspot.com/todo_items
