# server


## Project setup (Python 3.7)
Write in terminal for creating virtual environment and load necessary libs
```
pip install virtualenv
cd project_folder
virtualenv -p python3 .env
.env\Scripts\activate (for Windows)
(.env)$ pip install -r requirements.txt
```
Run server
```
python run.py
```
Mysql db for local usage - dump_todo.sql