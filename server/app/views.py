import logging
from flask import abort, json, request, jsonify
from app import app, db, models


@app.route('/todo_items', methods=['GET', 'POST'])
def todo_list():
    response_object = {'status': 'success'}
    if request.method == 'POST':
        post_data = request.get_json()
        if post_data is None:
            response_object['message'] = 'Cannot add empty task!'
        else:
            todo_item = models.Todo(**post_data)
            db.session.add(todo_item)
            db.session.commit()
            response_object['message'] = 'Item added!'
    else:
        response_object['todo_list'] = [i.serialize() for i in models.Todo.query.all()]

    return jsonify(response_object)


@app.route('/todo_items/<item_id>', methods=['PUT', 'DELETE'])
def todo_item(item_id):
    response_object = {'status': 'success'}
    if request.method == 'PUT':
        post_data = request.get_json()
        models.Todo.query.filter_by(id=item_id).update(post_data)
        db.session.commit()
        response_object['message'] = 'Todo item updated!'
    if request.method == 'DELETE':
        models.Todo.query.filter_by(id=item_id).delete()
        db.session.commit()
        response_object['message'] = 'Todo item removed!'
    return jsonify(response_object)
