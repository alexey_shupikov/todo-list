from app import db


class Todo(db.Model):
    __tablename__ = 'todo'
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(200), nullable=False)
    complete = db.Column(db.Boolean, default=False)

    def serialize(self):
        return {
            'id': self.id,
            'text': self.text,
            'complete': self.complete,
        }
