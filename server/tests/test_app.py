import pytest
from flask import json
from flask_sqlalchemy import SQLAlchemy

from app import app

db = SQLAlchemy(app)


class Todo(db.Model):
    __tablename__ = 'todo_test'
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(200), nullable=False)
    complete = db.Column(db.Boolean, default=False)

    def serialize(self):
        return {
            'id': self.id,
            'text': self.text,
            'complete': self.complete,
        }


def test_todo_list_add():
    # test if it works when on post getting empty
    response = app.test_client().post(
        '/todo_items',
        content_type='application/json',
    )
    data = json.loads(response.get_data(as_text=True))

    assert response.status_code == 200
    assert data['message'] == 'Cannot add empty task!'
    assert data['status'] == 'success'


@pytest.mark.parametrize(
    "text",
    (
            '',
            'Some test text',
    )
)
def test_todo_list_add(text):
    # test if it works when on post getting empty text
    response = app.test_client().post(
        '/todo_items',
        data=json.dumps({'text': text}),
        content_type='application/json',
    )
    data = json.loads(response.get_data(as_text=True))

    assert response.status_code == 200
    assert data['status'] == 'success'


def test_todo_list():
    response = app.test_client().get(
        '/todo_items',
        content_type='application/json',
    )
    data = json.loads(response.get_data(as_text=True))

    assert response.status_code == 200
    assert data['status'] == 'success'


@pytest.mark.parametrize(
    "path",
    (
            'Sometesttext',
            '-1',
            '5'
    )
)
def test_todo_item_delete(path):
    # test on critical errors
    response = app.test_client().delete(
        '/todo_items/' + path,
        content_type='application/json',
    )
    data = json.loads(response.get_data(as_text=True))

    assert response.status_code == 200
    assert data['message'] == "Todo item removed!"
    assert data['status'] == 'success'


def test_todo_item_update_text():
    response = app.test_client().post(
        '/todo_items/1',
        data=json.dumps({'text': 'Test task'}),
        content_type='application/json',
    )
    data = json.loads(response.get_data(as_text=True))

    assert response.status_code == 200
    assert data['message'] == 'Todo item updated!'
    assert data['status'] == 'success'


def test_todo_item_update_complete():
    response = app.test_client().post(
        '/todo_items/1',
        data=json.dumps({'complete': True}),
        content_type='application/json',
    )
    data = json.loads(response.get_data(as_text=True))

    assert response.status_code == 200
    assert data['message'] == 'Todo item updated!'
    assert data['status'] == 'success'
